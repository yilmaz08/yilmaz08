<p align="center">
  <samp>
    <a href="https://ayilmaz.xyz">website</a> -
    <a href="mailto:ayilmaz@ayilmaz.xyz">mail</a> -
    <a href="https://ayilmaz.xyz/ayilmaz.gpg">pgp</a> -
    <a href="https://github.com/yilmaz08/config">config</a> -
    <a href="https://github.com/yilmaz08">github</a> -
    <a href="https://x.com/ayilmazdev">twitter</a> -
    <a href="https://mastodon.social/@ayilmaz">mastodon</a> -
    <a href="https://go.ayilmaz.xyz/hidden">hidden</a>
  </samp>
</p>
